<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AntenaController;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('auth.login');
});

/*
Route::get('/antena', function () {
    return view('antena.index');
});


Route::get('/antena/create',[AntenaController::class, 'create']); // así solo puedo acceder al método create 
*/
Route::resource('antena', AntenaController::class)->middleware('auth'); //con esto puedo acceder a todos las URL y trabajar con todos los metodos y con middleware está respetando la autentificación
                                                
//Auth::routes(['register'=>false, 'reset'=>false]); // Con esto quitamos la opción de registro y resetear contraseña

Auth::routes(['reset'=>false]);

//Auth::routes();

Route::get('/home', [AntenaController::class, 'index'])->name('home');

Route::group(['middleware'=>'auth'], function(){
    Route::get('/', [AntenaController::class, 'index'])->name('home');
});
