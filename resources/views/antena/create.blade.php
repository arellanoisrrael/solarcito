@extends('layouts.app')
@section('content')
<div class="container">
    <form action="{{url('/antena')}}" method="post" enctype="multipart/form-data"> 
        @csrf
        @include('antena.form',['modo'=>'Crear']);   
    </form>
</div>
@endsection 