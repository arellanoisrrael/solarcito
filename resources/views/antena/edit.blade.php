@extends('layouts.app')
@section('content')
<div class="container">
    <form action="{{url('/antena/'.$antena->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        {{method_field('PATCH')}}  <!--Enviamos al controlador update-->
        @include('antena.form',['modo'=>'Editar']); 
    </form>
</div>
@endsection
