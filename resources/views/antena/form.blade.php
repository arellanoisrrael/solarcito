<h1>{{$modo}} antena</h1>

<!--VALIDACIONES-->
@if(count($errors)>0)
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
<!--FIN VALIDACIONES-->

<div class="form-group">
<label for="Ubicacion">Ubicación de la antena</label>
<input type="text" class="form-control" name="Ubicacion" value="{{isset($antena->Ubicacion)?$antena->Ubicacion:old('Ubicacion')}}" id="Ubicacion"> 
</div>

<!--form estado
<div class="form-group">
    <label for="Estado">Estado</label>
    <input type="text" class="form-control" name="Estado" value="{{isset($antena->Estado)?$antena->Estado:old('Estado')}}" id="Estado"> 
</div>
-->
<!--form select-->
<div class="form-group">
    <label for="Estado">Estado</label>
    <select class="form-select" aria-label="Default select example" name="Estado" value="{{isset($antena->Estado)?$antena->Estado:old('Estado')}}" id="Estado">
    <option selected>Estado</option>
    <option value="Activo">Activo</option>
    <option value="Inactivo">Inactivo</option>
    </select>
</div>
<!--fin form select-->


<div class="form-group">
<label for="Mapa">Mapa</label>
<input type="text" class="form-control" name="Mapa" value="{{isset($antena->Mapa)?$antena->Mapa:old('Mapa')}}" id="Mapa"> 
</div>

<div class="form-group">
<label for="IP">Dirección IP</label>
    <input type="text" class="form-control" name="IP" value="{{isset($antena->IP)?$antena->IP:old('IP')}}" id="IP"> 
</div>

<div class="form-group">
<label for="Foto">Foto de la Antena</label> <br>
@if(isset($antena->Foto))
<img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$antena->Foto}}" width="100" height="100" alt="">
@endif
<input type="file" class="form-control" name="Foto" value="" id="Foto"> 
</div>

<br>
<input class="btn btn-success" type="submit" value="{{$modo}} datos">

<a class="btn btn-primary" href="{{url('antena/')}}"> Regresar</a>
<br>