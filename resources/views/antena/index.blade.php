@extends('layouts.app')

@section('content')
<div class="container">

<!--Mensaje de alerta-->
    @if(Session::has('mensaje'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
    {{ Session::get('mensaje')}}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
<!--FIN Mensaje de alerta-->


<a href="{{url('antena/create')}}" class="btn btn-success"> Registrar nueva Antena</a>
<br>
<br>


<table class="table table-light">
    <thead class="table.light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Ubicación</th>
            <th>Mapa</th>
            <th>Dirección IP</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($antenas as $antena)
        <tr>
            <!--<td scope="row"></td> -->
            <td>{{$antena->id}}</td>

            <td>
                <img class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$antena->Foto}}" width="100" height="100" alt="">  <!--Le decimos que nos muestre la infromacion de la foto almacenada en Storage-->
                <!--{{$antena->Foto}}--> 
            </td>
            <td>{{$antena->Ubicacion}}</td>
            <td><a href="{{$antena->Mapa}}" target="_blank">{{$antena->Mapa}}</a></td>
            
            <td><a href="http://{{$antena->IP}}" target="_blank">{{$antena->IP}}</a></td>
            <td>{{$antena->Estado}}</td>
            <td>
                 <a href="{{url('/antena/'.$antena->id.'/edit')}}" class="btn btn-warning">
                    Editar
                </a>| 

                <form action="{{url('/antena/'.$antena->id)}}" class="d-inline" method="post">
                @csrf
                {{method_field('DELETE')}}
                <input  class="btn btn-danger" type="submit" onclick="return confirm('¿Quiéres borrar?')" value="Borrar">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{!! $antenas->links()!!}
</div>
@endsection
