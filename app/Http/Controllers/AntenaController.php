<?php

namespace App\Http\Controllers;

use App\Models\Antena;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AntenaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datos['antenas'] = Antena::paginate(5);
        return view('antena.index', $datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('antena.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDACIONES
        $campos=[
            'Ubicacion'=>'required|string| max:100',
            'Estado'=>'required|string| max:100',
            'Mapa'=>'required|string| max:100',
            'IP'=>'required|string| max:100',
            'Ubicacion'=>'required|string| max:100',
            'Foto'=>'required| max:10000|mimes: jpeg,png,jpg',
            
        ];
        $mensaje=[
            'required'=>'El :attribute  es requerido',
            'Foto.required'=>'La foto es requerida'
        ];
        $this->validate($request, $campos, $mensaje);
        //FIN VALIDACIONES


        //$datosAntena = request()->all(); //recepciona todos los datos enviados por el método POST
        $datosAntena = request()->except('_token'); // recolecta toda la información excepto el token

        if ($request->hasFile('Foto')) {
            $datosAntena['Foto'] = $request->file('Foto')->store('uploads', 'public');
        }

        Antena::insert($datosAntena); // lo vamos a insertar directamente a la base de datos

        //return response()->json($datosAntena); //retornamos los datos que nos enviaron
        return redirect('antena')->with('mensaje', 'Antena agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Antena  $antena
     * @return \Illuminate\Http\Response
     */
    public function show(Antena $antena)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Antena  $antena
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $antena = Antena::findOrFail($id);
        return view('antena.edit', compact('antena'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Antena  $antena
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //VALIDACIONES
        $campos=[  //validamos que los campos estén vacíos
            'Ubicacion'=>'required|string| max:100',
            'Estado'=>'required|string| max:100',
            'Mapa'=>'required|string| max:100',
            'IP'=>'required|string| max:100',
            'Ubicacion'=>'required|string| max:100',
            
            
        ];
        $mensaje=[
            'required'=>'El :attribute  es requerido',
            'Foto.required'=>'La foto es requerida'
        ];
        if ($request->hasFile('Foto')) { 
            $campos=['Foto'=>'required| max:10000|mimes: jpeg,png,jpg'];
            $mensaje=['Foto.required'=>'La foto es requerida'];
        }

        $this->validate($request, $campos, $mensaje);
        //FIN VALIDACIONES


        $datosAntena = request()->except(['_token', '_method']); // recolecta toda la información excepto el token y method

        if ($request->hasFile('Foto')) {  //pregunta si la información foto existe
            $antena = Antena::findOrFail($id);
            Storage::delete(['public/'.'$antena->Foto']); // estamos borrando esa información

            $datosAntena['Foto'] = $request->file('Foto')->store('uploads','public'); // si hubo ese cambio pues actualizamos la información
        }

        Antena::where('id', '=',$id)->update($datosAntena); // hacemos la actualización 
        //si hago la actualización pues volvemos al formulario 
        $antena = Antena::findOrFail($id);
        //return view('antena.edit',compact('antena'));
        return redirect('antena')->with('mensaje', 'Antena Modificado'); //retornamos a antena despues de borrar
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Antena  $antena
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $antena = Antena::findOrFail($id); //busca la información a través de una consulta
        if (Storage::delete('public/'.$antena->Foto)) { //preguntamos si ya se borró la foto
            Antena::destroy($id); // si ya se borró pues destruimos el registro
        }
        return redirect('antena')->with('mensaje', 'Antena Borrado'); //retornamos a antena despues de borrar
    }
}
